FROM alpine:3.19.3

LABEL maintainer="PapierPain <gourves.seven@gmail.com>"
LABEL description="Alpine container with usefull tools"

####################
# CONFIGURATION
####################

RUN apk add --no-cache --update curl jq bash python3 py3-pip && \
    pip3 install --break-system-packages --no-cache --upgrade pip setuptools jinja2-cli yq
